import Head from 'next/head'
import Link from 'next/link'

export default function Home() {
  return (
    <div>
      <Head>
        <link rel="stylesheet" href="https://fonts.xz.style/serve/inter.css" />
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@exampledev/new.css@1.1.2/new.min.css" />
      </Head>
      <header>
        <h1>Mauro Gestoso</h1>
      </header>
      <main>
        <ul>
          <li><Link href="/cv"><a>/cv</a></Link></li>
        </ul>
      </main>
    </div>
  )
}
